import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { GetServerSideProps } from 'next';
import styled from 'styled-components';
import Image from 'next/image';
import { Product } from '../../../store/reducers/types';
import { Title } from '../../../components/Title';
import { Main } from '../../../components/Main';
import { Button, Intent } from '@blueprintjs/core';
import { Price } from '../../../components/Price';
import { RatingComponent } from '../../../components/Rating';
import { Description } from '../../../components/Description';
import { updateShoppingCart } from '../../../store/actions/updateShoppingCart';
import { getShoppingCart } from '../../../store/selectors/shoppingCart';
import { ImageContainer } from '../../../components/ImageContainer';

const ContentContainer = styled.div`
  display: flex;
  flex-grow: 1;
  padding: 42px;
`;

const DetailsContainer = styled.div`
  flex: 1;
  border-left: 1px;
  border-left: 2px solid lightgrey;
  padding-left: 32px;
`;

const StyledButton = styled(Button)`
  margin-top: 16px;
`;

const NavigateBackButton = styled(Button)`
  width: 100px;
  position: absolute;
  margin-top: -16px;
`;

const StyledImageContainer = styled(ImageContainer)`
  width: calc(50% - 32px);
  margin-right: 32px;
`;

const ProductDetails = ({ product }: { product: Product }) => {
  const dispatch = useDispatch();
  const shoppingCartItems = useSelector(getShoppingCart);
  const router = useRouter();

  const addToCard = useCallback(() => {
    const alreadyExists = shoppingCartItems.some(({ id }) => id === product.id);
    if (alreadyExists) {
      dispatch(
        updateShoppingCart({
          updatedShoppingCart: shoppingCartItems.map((item) => {
            return item.id === product.id
              ? { ...item, quantity: item.quantity + 1 }
              : item;
          }),
        })
      );
    } else {
      dispatch(
        updateShoppingCart({
          updatedShoppingCart: [
            ...shoppingCartItems,
            { ...product, quantity: 1 },
          ],
        })
      );
    }
  }, [product, shoppingCartItems, dispatch]);

  return (
    <Main>
      <NavigateBackButton
        text="Go back"
        onClick={() => router.back()}
        icon={'arrow-left'}
      />
      <Title>{product.title}</Title>
      <ContentContainer>
        <StyledImageContainer>
          <Image
            src={product.image}
            layout="fill"
            objectFit="contain"
            alt={product.title}
          />
        </StyledImageContainer>
        <DetailsContainer>
          <Price price={product.price} size="32px" />
          <RatingComponent rating={product.rating} />
          <Description text={product.description} />
          <StyledButton
            text="Add to Cart"
            large
            intent={Intent.PRIMARY}
            onClick={addToCard}
          />
        </DetailsContainer>
      </ContentContainer>
    </Main>
  );
};

export default ProductDetails;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { params } = context;
  const response = await fetch(
    `https://fakestoreapi.com/products/${params.productId}`
  );
  const data = await response.json();

  return {
    props: {
      product: data,
    },
  };
};
