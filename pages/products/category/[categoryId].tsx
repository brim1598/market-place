import { GetServerSideProps } from 'next';
import { Product } from '../../../store/reducers/types';
import ProductList from '..';

type Props = {
  products: Product[];
  categoryId: string;
};

const ProductsByCategory = ({ products, categoryId }: Props) => (
  <ProductList products={products} title={`Products for ${categoryId}`} />
);

export default ProductsByCategory;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { params } = context;
  const response = await fetch(
    `https://fakestoreapi.com/products/category/${params.categoryId}`
  );
  const data = await response.json();

  return {
    props: {
      products: data,
      categoryId: params.categoryId,
    },
  };
};
