import { Card } from '@blueprintjs/core';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useCallback } from 'react';
import styled from 'styled-components';
import { ImageContainer } from '../../components/ImageContainer';
import { Main } from '../../components/Main';
import { Price } from '../../components/Price';
import { Title } from '../../components/Title';
import { MAX_CHAR_NUMBER_OF_PRODUCT_TITLE_IN_LIST_VIEW } from '../../constants';
import { Product } from '../../store/reducers/types';
import { truncate } from '../../utils';

type Props = {
  products: Product[];
  title: string;
};

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  place-content: center;
`;

const StyledCard = styled(Card)`
  width: 310px;
  height: 420px;
  padding: 16px;
  margin: 32px;
  display: grid;
  grid-template-rows: 72% 22% 6%;
  text-align: center;
`;

const ProductName = styled.h5`
  font-size: 14px;
  margin-top: 12px;
  margin-bottom: 12px;
  line-height: 1.5;
`;

const ProductList = ({ products, title }: Props) => {
  const router = useRouter();
  const redirectToProductPage = useCallback(
    (id: number) => {
      router.push(`/products/${id}`);
    },
    [router]
  );

  return (
    <Main>
      <Title>{title}</Title>
      <Container>
        {products.map((product) => (
          <StyledCard interactive key={product.id}>
            <ImageContainer onClick={() => redirectToProductPage(product.id)}>
              <Image
                src={product.image}
                layout="fill"
                objectFit="contain"
                alt={product.title}
              />
            </ImageContainer>
            <ProductName onClick={() => redirectToProductPage(product.id)}>
              {truncate(
                product.title,
                MAX_CHAR_NUMBER_OF_PRODUCT_TITLE_IN_LIST_VIEW
              )}
            </ProductName>
            <Price price={product.price} />
          </StyledCard>
        ))}
      </Container>
    </Main>
  );
};

export default ProductList;

export const getServerSideProps = async () => {
  const response = await fetch('https://fakestoreapi.com/products');
  const data = await response.json();

  return {
    props: {
      products: data,
      title: 'All Products',
    },
  };
};
