import '../styles/globals.scss';
import { Provider } from 'react-redux';
import { store } from '../store/store';
import { NavigationBar } from '../components/NavigationBar';
import { Footer } from '../components/Footer';

const MyApp = ({ Component, pageProps }) => (
  <Provider store={store}>
    <div className="wrapper">
      <NavigationBar />
      <Component {...pageProps} />
      <Footer />
    </div>
  </Provider>
);

export default MyApp;
