import { Card } from '@blueprintjs/core';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useCallback } from 'react';
import styled from 'styled-components';
import { ImageContainer } from '../components/ImageContainer';
import { Main } from '../components/Main';

const CARD_HEIGHT = '12em';

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: ${CARD_HEIGHT} ${CARD_HEIGHT};
  grid-gap: 32px;
  padding: 32px;
`;

const StyledCard = styled(Card)`
  padding: 0;
`;

const StyledImageContainer = styled(ImageContainer)`
  height: ${CARD_HEIGHT};
  filter: blur(1px) brightness(0.5);
  z-index: 0;
`;

const StyledH5 = styled.h5`
  z-index: 1;
  position: absolute;
  color:white;
  margin:0.5em;
  font-size:32px;
  font-variant: all-petite-caps;
  text-shadow: rgb(0 0 0 / 80%) 0px 1px 5px;
}
`;

const Home = ({ categories }: { categories: string[] }) => {
  const router = useRouter();

  const onClick = useCallback(
    (category: string) => {
      router.push(`/products/category/${category}`);
    },
    [router]
  );

  return (
    <Main>
      <GridContainer>
        {categories.map((category) => (
          <StyledCard
            key={category}
            interactive
            onClick={() => onClick(category)}
          >
            <StyledH5>{category}</StyledH5>
            <StyledImageContainer>
              <Image
                src={`/${category}.jpg`}
                alt={category}
                layout="fill"
                objectFit="cover"
                objectPosition="center"
              />
            </StyledImageContainer>
          </StyledCard>
        ))}
      </GridContainer>
    </Main>
  );
};

export default Home;

export const getStaticProps = async () => {
  const response = await fetch('https://fakestoreapi.com/products/categories');
  const data = await response.json();

  return {
    props: {
      categories: data,
    },
    revalidate: 60,
  };
};
