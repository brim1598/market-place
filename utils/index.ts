export const truncate = (text: string, maxCharNumber: number) => {
  return text.length > maxCharNumber
    ? text.substring(0, maxCharNumber) + '...'
    : text;
};
