import styles from '../styles/Description.module.scss';

export const Description = ({ text }: { text: string }) => (
  <p className={styles.description}>{text}</p>
);
