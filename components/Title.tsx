import styles from '../styles/Title.module.scss';

export const Title = ({ children }: { children: JSX.Element | string }) => (
  <h1 className={styles.title}>{children}</h1>
);
