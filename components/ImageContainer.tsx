import styles from '../styles/ImageContainer.module.scss';

type Props = {
  children: JSX.Element;
  onClick?: () => void;
  className?: string;
};

export const ImageContainer = ({ children, onClick, className }: Props) => (
  <div
    className={`${styles['image-container']} ${className}`}
    onClick={onClick}
  >
    {children}
  </div>
);
