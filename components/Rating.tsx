import { Icon, IconSize } from '@blueprintjs/core';
import { MAX_RATE_NUMBER } from '../constants';
import { Rating } from '../store/reducers/types';
import styles from '../styles/Rating.module.scss';

type Props = {
  rating: Rating;
};

export const RatingComponent = ({ rating: { count, rate } }: Props) => {
  const roundNumber = Math.round(rate);

  return (
    <div className={styles.container}>
      <div className={styles['rating-bar']}>
        {[
          Array.from({ length: roundNumber }, (_, i) => i).map((index) => (
            <Icon icon="star" size={IconSize.STANDARD} key={index} />
          )),
          Array.from(
            { length: MAX_RATE_NUMBER - roundNumber },
            (_, i) => roundNumber + i
          ).map((index) => (
            <Icon icon="star-empty" size={IconSize.STANDARD} key={index} />
          )),
        ]}
        <span className={styles.rate}>{rate}</span>
      </div>
      <span className={styles.rate}>{count} Review</span>
    </div>
  );
};
