import styled from 'styled-components';
import { CURRENCY } from '../constants';
import styles from '../styles/Price.module.scss';

type Props = {
  price: number;
  size?: string;
};

const Container = styled.span<{ size?: string }>`
  ${(props) => props.size && `font-size: ${props.size};`}
`;

export const Price = ({ price, size }: Props) => (
  <Container className={styles.price} size={size}>
    {price} {CURRENCY}
  </Container>
);
