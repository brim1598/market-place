import { COMPANY_NAME } from '../constants';
import styles from '../styles/Footer.module.scss';

export const Footer = () => (
  <footer className={styles.footer}>
    <p>
      © {COMPANY_NAME} {new Date().getFullYear()}
    </p>
  </footer>
);
