import { Alignment, Navbar, Button, Icon } from '@blueprintjs/core';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { COMPANY_NAME } from '../constants';
import { getShoppingCart } from '../store/selectors/shoppingCart';
import styles from '../styles/NavigationBar.module.scss';

export const NavigationBar = () => {
  const cartItems = useSelector(getShoppingCart);
  const amountOfItems = cartItems.reduce(
    (sum, { quantity }) => sum + quantity,
    0
  );
  const router = useRouter();

  return (
    <Navbar className={styles.navbar}>
      <Navbar.Group
        align={Alignment.LEFT}
        className={styles['header-logo-container']}
      >
        <Navbar.Heading
          onClick={() => router.push('/')}
          className={styles['header-logo']}
        >
          {COMPANY_NAME}
        </Navbar.Heading>
        <Navbar.Divider />
      </Navbar.Group>
      <Navbar.Group align={Alignment.RIGHT}>
        <Button
          className="bp4-minimal"
          icon="home"
          text="Home"
          onClick={() => router.push('/')}
        />
        <Navbar.Divider />
        <Button
          className="bp4-minimal"
          icon="shop"
          text="Products"
          onClick={() => router.push('/products')}
        />
        <Navbar.Divider />
        <div className={styles['shopping-cart-container']}>
          <Icon icon="shopping-cart" title="Shopping Cart" />
          <span>({amountOfItems})</span>
        </div>
      </Navbar.Group>
    </Navbar>
  );
};
