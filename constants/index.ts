export const COMPANY_NAME = 'Thrift Master';
export const CURRENCY = 'HUF';

export const MAX_CHAR_NUMBER_OF_PRODUCT_TITLE_IN_LIST_VIEW = 100;
export const MAX_RATE_NUMBER = 5;
