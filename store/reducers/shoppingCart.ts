import {
  UpdateShoppingCartAction,
  UPDATE_SHOPPING_CART_ACTION_TYPE,
} from '../actions/updateShoppingCart';
import type { ShoppingCartItem } from './types';

const initialState: ShoppingCartItem[] = [];

export const shoppingCart = (
  state = initialState,
  action: UpdateShoppingCartAction
): ShoppingCartItem[] => {
  switch (action.type) {
    case UPDATE_SHOPPING_CART_ACTION_TYPE.UPDATE_SHOPPING_CART: {
      const { updatedShoppingCart } = action;
      return updatedShoppingCart;
    }
    default: {
      return state;
    }
  }
};
