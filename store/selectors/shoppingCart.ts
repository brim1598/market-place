import { RootState } from "../reducers/types";

export const getShoppingCart = (state: RootState) => state.shoppingCart;
