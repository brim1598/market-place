import { ShoppingCartItem } from '../reducers/types';

export enum UPDATE_SHOPPING_CART_ACTION_TYPE {
  UPDATE_SHOPPING_CART = 'UPDATE_SHOPPING_CART',
}

export type UpdateShoppingCartAction = {
  updatedShoppingCart: ShoppingCartItem[];
  type: UPDATE_SHOPPING_CART_ACTION_TYPE.UPDATE_SHOPPING_CART;
};

export const updateShoppingCart = ({
  updatedShoppingCart,
}: Omit<UpdateShoppingCartAction, 'type'>): UpdateShoppingCartAction => ({
  type: UPDATE_SHOPPING_CART_ACTION_TYPE.UPDATE_SHOPPING_CART,
  updatedShoppingCart,
});
